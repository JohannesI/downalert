import WatchList from "@/services/WatchList";
import Storage from "@/services/Storage";
import MockStorageApi from "../../../mocks/StorageApi";

import { Site } from "@/definitions/Site";

describe("WatchList service", () => {
    it("Adds new sites", async () => {
        const mockedStorage = new MockStorageApi({}),
            watchList = new WatchList(new Storage(mockedStorage));

        const newSite: Site = {
            url: "https://google.com",
            latencies: [],
        };
        await watchList.addSite(newSite.url);

        expect(mockedStorage.local._storage.sites).toStrictEqual([newSite]);
    });

    it("Does not add duplicate sites", async () => {
        const mockedStorage = new MockStorageApi({}),
            watchList = new WatchList(new Storage(mockedStorage));

        await watchList.addSite("https://google.com");
        await watchList.addSite("https://bing.com");
        await watchList.addSite("https://google.com");
        await watchList.addSite("https://bing.com");

        expect(mockedStorage.local._storage.sites.length).toBe(2);
    });

    it("Removes sites", async () => {
        const mockedStorage = new MockStorageApi({}),
            watchList = new WatchList(new Storage(mockedStorage));

        await watchList.addSite("https://google.com");
        await watchList.addSite("https://bing.com");
        await watchList.removeSite("https://google.com");

        expect(mockedStorage.local._storage.sites.length).toBe(1);
        expect(mockedStorage.local._storage.sites[0].url).toBe(
            "https://bing.com"
        );
    });

    it("Retrieves all sites", async () => {
        const sitesInStorage: Site[] = [
            { url: "https://google.com", latencies: [] },
            { url: "https://bing.com", latencies: [] },
        ];

        const mockedStorage = new MockStorageApi({ sites: sitesInStorage }),
            watchList = new WatchList(new Storage(mockedStorage));

        const sites = await watchList.getAllSites();

        expect(sites).toStrictEqual(sitesInStorage);
    });

    it("Can check if a site is already in the watchlist", async () => {
        const sitesInStorage: Site[] = [
            { url: "https://google.com", latencies: [] },
        ];

        const mockedStorage = new MockStorageApi({ sites: sitesInStorage }),
            watchList = new WatchList(new Storage(mockedStorage));

        expect(await watchList.contains("https://google.com")).toBe(true);
        expect(await watchList.contains("https://bing.com")).toBe(false);
    });

    it("Allows latencies to be set", async () => {
        const sitesInStorage: Site[] = [
            { url: "https://google.com", latencies: [] },
        ];

        const mockedStorage = new MockStorageApi({
                sites: sitesInStorage,
            }),
            watchList = new WatchList(new Storage(mockedStorage));

        const latencies = [
            {
                latency: 418,
                startTime: 1634998457329,
                successful: true,
            },
        ];

        await watchList.setLatencyOfSite("https://google.com", latencies);

        const latenciesInStorage =
            mockedStorage.local._storage.sites[0].latencies;
        expect(latenciesInStorage).toStrictEqual(latencies);
    });

    it("Keeps track of new sites", async () => {
        const mockedStorage = new MockStorageApi({}),
            watchList = new WatchList(new Storage(mockedStorage));

        const updateCallback = jest.fn();
        watchList.listenForUpdates(updateCallback);

        await watchList.addSite("https://google.com");

        expect(updateCallback).toBeCalledWith([
            { url: "https://google.com", latencies: [] },
        ]);

        await watchList.removeSite("https://google.com");

        expect(updateCallback).toHaveBeenLastCalledWith([]);
        expect(updateCallback).toHaveBeenCalledTimes(2);
    });
});
