import Storage from "@/services/Storage";
import MockStorageApi from "../../../mocks/StorageApi";

describe("Storage service", () => {
    it("Reads from storage", async () => {
        const storedData = {
            username: "Liam123",
            email: "liam123@gmail.com",
        };

        const mockedStorage = new MockStorageApi(storedData),
            storage = new Storage(mockedStorage);

        const { username, email } = await storage.read(["username", "email"]);
        expect(username).toEqual(storedData.username);
        expect(email).toEqual(storedData.email);
    });

    it("Writes to storage", async () => {
        const mockedStorage = new MockStorageApi({}),
            storage = new Storage(mockedStorage);

        await storage.write({ name: "Steve" });

        expect(mockedStorage.local._storage.name).toEqual("Steve");
    });

    it("Reads written storage", async () => {
        const mockedStorage = new MockStorageApi({}),
            storage = new Storage(mockedStorage);

        await storage.write({ userId: 42 });

        const { userId } = await storage.read(["userId"]);
        expect(userId).toEqual(42);
    });

    it("Observes changes to storage", async () => {
        const mockedStorage = new MockStorageApi({}),
            storage = new Storage(mockedStorage);

        const watchCallback = jest.fn();
        storage.watch("displayName", watchCallback);

        await storage.write({ displayName: "Steve123" });

        expect(watchCallback).toHaveBeenCalledWith("Steve123");
    });
});
