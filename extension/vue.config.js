// For building the popup
const CopyWebpackPlugin = require("copy-webpack-plugin"); // eslint-disable-line @typescript-eslint/no-var-requires
const path = require("path"); // eslint-disable-line @typescript-eslint/no-var-requires

const pages = {
    popup: {
        entry: "src/popup/main.ts",
        template: "public/index.html",
        filename: "popup.html",
    },
};

const configureWebpack = {
    devtool: "cheap-module-source-map",
    plugins: [
        new CopyWebpackPlugin([
            {
                from: path.resolve("src/manifest.json"),
                to: `${path.resolve("dist")}/manifest.json`,
            },
        ]),
    ],
};

module.exports = {
    configureWebpack,
    pages,
};
