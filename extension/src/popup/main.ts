import { createApp } from "vue";
import App from "./App.vue";

import Storage from "../services/Storage";
import WatchList from "../services/WatchList";
import { Site } from "@/definitions/Site";

const app = createApp(App);

// Add the watchList as a global property
app.config.globalProperties.$watchList = new WatchList(
    new Storage(chrome.storage)
);

app.mount("#app");

interface watchList {
    addSite(siteUrl: string): Promise<void>;
    contains(siteUrl: string): Promise<boolean>;
    getAllSites(): Promise<Site[]>;
    removeSite(siteUrl: string): Promise<void>;
    listenForUpdates(callback: (newValues: Site[]) => void): void;
}

// This is to make sure the linter doesn't go crazy saying the $watchList property doesn't exist
declare module "@vue/runtime-core" {
    interface ComponentCustomProperties {
        $watchList: watchList;
    }
}
