interface StorageModule {
    write: (object: { [key: string]: unknown }) => Promise<boolean>;
    read: (keys: string[]) => Promise<{ [key: string]: unknown }>;
    watch: (keyToWatch: string, callback: (newValues: unknown) => void) => void;
}

import { Site } from "../definitions/Site";
import { LatencyCheck } from "../definitions/LatencyCheck";

// Class that handles storing and management of sites that are watched
export default class WatchList {
    constructor(storage: StorageModule) {
        this.storage = storage;

        // Listen for new sites being added to the storage
        const callbacks = this.callbacks;
        this.storage.watch("sites", (newValues: unknown) => {
            this.sites = newValues as Site[];
            for (const callback of callbacks) {
                callback(newValues as Site[]);
            }
        });
    }

    public async addSite(siteUrl: string): Promise<void> {
        // Make sure the url is external
        if (!siteUrl.includes("http")) {
            return;
        }

        const sites = await this.getAllSites();

        for (const site of sites) {
            if (site.url === siteUrl) {
                return;
            }
        }

        const newSite: Site = {
            url: siteUrl,
            latencies: [],
        };
        sites.push(newSite);

        await this.storage.write({ sites });
    }

    public async removeSite(siteUrl: string): Promise<void> {
        const sites = await this.getAllSites();

        for (const index in sites) {
            if (sites[index].url == siteUrl) {
                sites.splice(parseInt(index), 1);
                await this.storage.write({ sites });
                return;
            }
        }
    }

    public async getAllSites(): Promise<Site[]> {
        if (this.sites.length > 0) {
            return this.sites;
        }

        const { sites = [] } = await this.storage.read(["sites"]);
        this.sites = sites as Site[];
        return sites as Site[];
    }

    public async contains(siteUrl: string): Promise<boolean> {
        const sites = await this.getAllSites();

        for (const site of sites) {
            if (site.url == siteUrl) {
                return true;
            }
        }

        return false;
    }

    public async setLatencyOfSite(
        siteUrl: string,
        latencies: LatencyCheck[]
    ): Promise<void> {
        const sites = await this.getAllSites();
        for (const site of sites) {
            if (site.url == siteUrl) {
                site.latencies = latencies;
            }
        }

        await this.storage.write({ sites });
    }

    listenForUpdates(callback: (sites: Site[]) => void): void {
        this.callbacks.push(callback);
    }

    private sites: Site[] = [];
    private callbacks: ((newValues: Site[]) => void)[] = [];
    private storage: StorageModule;
}
