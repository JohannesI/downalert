// Interface (not in the TypeScript sense) for Chrome Extension local storage
export default class Storage {
    constructor(storageApi: any) {
        this.storageApi = storageApi;
    }

    private storageApi: any;

    public async read(keys: string[]): Promise<{ [key: string]: unknown }> {
        return new Promise((resolve, reject) => {
            try {
                this.storageApi.local.get(
                    keys,
                    (results: { [key: string]: unknown }) => {
                        resolve(results);
                    }
                );
            } catch (error) {
                console.error(error);
                reject(error);
            }
        });
    }

    public async write(object: { [key: string]: unknown }): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                this.storageApi.local.set(object, () => {
                    resolve(true);
                });
            } catch (error) {
                console.error(error);
                reject(error);
            }
        });
    }

    public watch(
        keyToWatch: string,
        callback: (newValues: unknown) => void
    ): void {
        this.storageApi.onChanged.addListener(
            (
                changes: {
                    [key: string]: chrome.storage.StorageChange;
                },
                area: "local" | "sync" | "managed"
            ) => {
                if (area == "local") {
                    if (changes[keyToWatch] != undefined) {
                        callback(changes[keyToWatch].newValue);
                    }
                }
            }
        );
    }
}
