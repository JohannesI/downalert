export type LatencyCheck = {
    latency: number;
    startTime: number;
    successful: boolean;
};
