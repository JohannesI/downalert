import { LatencyCheck } from "./LatencyCheck";

interface Site {
    url: string;
    latencies: LatencyCheck[];
}

export { Site };
