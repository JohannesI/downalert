type area = "local" | "sync" | "managed";

interface changeCallback {
    (changes: { key: chrome.storage.StorageChange }, area: area): void;
}

interface localStorage {
    _storage: any;
    get: (keys: string[], callback: (response: any) => void) => void;
    set: (items: any, callback: () => void) => void;

    registerChanges: (changes: any) => void;
    _changeCallbacks: changeCallback[];
}

interface onChanged {
    addListener: (
        callback: (
            changes: {
                key: chrome.storage.StorageChange;
            },
            area: area
        ) => void
    ) => void;

    _changeCallbacks: changeCallback[];
}

export default class MockStorageApi {
    constructor(content: any) {
        this.local._storage = content;
        this.local._changeCallbacks = this.onChanged._changeCallbacks;
    }

    local: localStorage = {
        _storage: {},
        get(keys: string[], callback: (response: any) => void): void {
            const response: any = {};

            for (const key of keys) {
                response[key] = this._storage[key];
            }

            callback(response);
        },
        set(items: any, callback: () => void): void {
            const changes: { [key: string]: any } = {};
            for (const key in items) {
                changes[key] = {
                    newValue: items[key],
                    oldValue: this._storage[key],
                };
                this._storage[key] = items[key];
            }
            this.registerChanges(changes);

            callback();
        },

        registerChanges(changes: any): void {
            for (const changeCallback of this._changeCallbacks) {
                changeCallback(changes, "local");
            }
        },

        _changeCallbacks: [],
    };

    onChanged: onChanged = {
        addListener(callback): void {
            this._changeCallbacks.push(callback);
        },

        _changeCallbacks: [],
    };
}
